# Contributing to FirefoxTweaks
Want to contribute to the project? That's awesome! If you're going to contribute, please open an issue first for discussion before making a pull request. Please provide reasons why you think your change is good. Finally, you may need to follow some rules below for your contribution to be accepted.

## 1. Addons
All addons must be free and open source. All addons must be available and functional on Firefox for Android. All addons must include links to the addon's source code, privacy policy, and install page on `addons.mozilla.org`. All addons must not collect any data. If you want to include additional files like a whitelist, put them in the root of the project.

## 2. uMatrix whitelist rules
Please create a separate issue for each primary domain. For example, if you had rules that fixed apple.com and microsoft.com, you would need to create two separate issues.

If you're fixing a completely new domain that's login-based (e.g. Netflix), please try to add a Cookie AutoDelete whitelist addition as well. Both of these changes may go into one issue.

If you're removing existing rules, please validate that the site still works with those domains blocked.

## 3. user.js tweaks
Try to avoid tweaks which cause problems (like `privacy.resistFingerprinting`). 

Please provide at least a basic description of the tweak in a comment.