# How to use uMatrix
The best description of what uMatrix is can be found on its AMO page [here](https://addons.mozilla.org/en-US/firefox/addon/umatrix/?src=search). Give the addon's description a read before coming back here and reading this guide. If you don't understand a lot of what it's saying, but still want to use uMatrix, that's okay -- but you should still try to read the description before continuing this guide.

What's the difference between first- and third-party items on a page, like cookies and scripts? First-party items are items from the domain you're visiting. For example, if you were on `theguardian.com` and it tried to load a script from `theguardian.com`, that script would be a first-party script. However, if you were on `theguardian.com` and it tried to load a script from `guim.co.uk`, that script would be considered third-party.

It can be helpful to think of uMatrix's default rules in terms of trust -- everything first-party is "trusted" because you chose to visit the site, but everything third-party is untrusted because you didn't explicitly tell your browser to visit those sites. uMatrix blocks almost everything that isn't third-party by default. The only third-party requests allowed are for CSS and images, because those are, for the most part, harmless. In contrast, uMatrix allows the first-party domain to do mostly whatever it wants, by default. As a consequence, website functionality often breaks, as a lot of websites depend on third-party resources. So in order to still get functionality out of websites, you have to "un-break" them by making your own uMatrix rules.

Let's start with an example. Say I'm trying to view a simple article on `theguardian.com`. It looks okay at first -- the images and text for the article have loaded -- but when I click an image in the article, it doesn't go full-screen.

Here's what the article and uMatrix look like, with the default rules:
<p align="center"><img src="https://gitlab.com/krathalan/firefox-tweaks/raw/master/ProjectImages/uMatrix1.jpg"></a>

uMatrix is telling us what's allowed in green, and what's blocked in red. The lighter red means it was blocked by the "most everything third-party is blocked" default rule, or the "top" domain was blocked -- `www.facebook.com` is blocked because `facebook.com` is blocked, for example. The dark red means that domain is in a blocklist used by uMatrix, or that domain is blocked explicitly in your uMatrix rules.

Right now, uMatrix is blocking 2 scripts and 2 XHR requests to `assets.guim.co.uk`, 1 image from `www.facebook.com`, 1 script from `www.google-analytics.com`, 1 image from `beacon.gu-web.net`, and 1 script from `sb.scorecardresearch.com`. If you don't know what a script or XHR request is, you can read more about what all of these are [here](https://github.com/gorhill/uMatrix/wiki/The-popup-panel#the-type-cells). 

Everything under the line with the small triangle is a blacklisted domain, meaning that in nearly all circumstances, you shouldn't trust those domains. It can be helpful to click the line with the triangle to collapse the blacklisted domains, since you shouldn't really enable anything from a blacklisted domain.

<p align="center"><img width="500" src="https://gitlab.com/krathalan/firefox-tweaks/raw/master/ProjectImages/uMatrix2.jpg"></a>

Now, how do I make clicking on images to expand them work? Well, it's a guessing game. Seeing as there's only one non-blacklisted top-level domain available, it probably has something to do with that. Since scripts provide site interaction and XHR requests usually provide "constantly updated" things like tickers or a live chat, I'm going to guess that clicking on images to expand them probably has to do more with scripts than XHR requests. So I'll enable scripts for `guim.co.uk`, reload the page, and try to click on the image again. And it works:
<p align="center"><img src="https://gitlab.com/krathalan/firefox-tweaks/raw/master/ProjectImages/uMatrix3.jpg"></a>

To save my rules so I don't have to do that every time I visit `theguardian.com`, I click the padlock in the top part of uMatrix.
<p align="center"><img src="https://gitlab.com/krathalan/firefox-tweaks/raw/master/ProjectImages/uMatrix4.jpg"></a>

And that's it! Every time I read an article on `theguardian.com` now, I'll be able to click on images to expand them (as long as `theguardian.com` don't change how their website works, of course). This was a simpler example, but should give you a good idea on how to fix websites. It's essentially a trial-and-error game.

If you prefer, you can textually edit your rules from the uMatrix options. To read about the syntax and how to add, remove, or edit your rules textually, refer to [this](https://github.com/gorhill/uMatrix/wiki/Rules-syntax) page in the uMatrix wiki.

[The project's Github Wiki](https://github.com/gorhill/uMatrix/wiki) contains a lot of useful information. [This page](https://github.com/gorhill/uMatrix/wiki/The-popup-panel) in particular is very helpful.