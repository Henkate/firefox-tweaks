# uBlock Origin third-party filter lists
## How to add a filter list
In uBlock Origin's settings, click the "Filter lists" tab at the top. Scroll all the way to the bottom, check the "Import..." checkbox, then add each URL you wish to add on a separate line.

## Trackers, advertisements, and other malicious behavior
[StevenBlack's lists](https://github.com/StevenBlack/hosts) are very good. There are alternate lists which include blocking for fake news, gambling sites, porn, and social sites. I've only included the base list below.

Base unified list: https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts

[anudeepND's lists](https://github.com/anudeepND/blacklist) are very good. 

Anti-coin miners: https://raw.githubusercontent.com/anudeepND/blacklist/master/CoinMiner.txt

Anti-ad: https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt

## Annoyances
While there are good annoyances lists available in uBlock Origin already, I have a (tiny) personal annoyance list.

WARNING: Using this list will remove functional elements on twitch.tv. Even though they're part of the site's function, I don't use them, therefore they're annoying, and blocked by this list.
Krathalan's Annoyances list: https://gitlab.com/krathalan/firefox-tweaks/raw/master/krathalans-annoyances.txt

## Fonts
Web fonts are typically served by a CDN, much like software libraries -- the difference is, you can use Decentraleyes to intercept those requests for software libraries and instead use Decentraleyes' local version, but there isn't a current analogue for web fonts. How much you trust CDNs versus how much you value a more aesthetically pleasing web is up to you. 

Note that by using an anti-third-party fonts list, you may actually cause some websites to break -- instead of where a hamburger or arrow button might be, there might be unclickable text or nothing at all. You can find information about how web fonts could affect your privacy (and security) [here](https://collinmbarrett.com/block-web-fonts/).

Fanboy's anti-third-party fonts list: https://fanboy.co.nz/fanboy-antifonts.txt

## List compilations
If you're itching for more lists, you can check [filterlists.com](https://filterlists.com/) and [WaLLy3K's blacklists](https://firebog.net/). WaLLy3K's lists are well known within the [Pi-hole](https://pi-hole.net/) community. However, be wary when using any of these lists: they may break some websites.