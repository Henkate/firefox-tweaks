# Reasoning
Here you may find my reasoning as to why I didn't include certain addons or user.js tweaks.

## Add-ons
### Ghostery
Ghostery used to be a proprietary addon, and the primary goal of the owners of the addon is to make money -- not protect your privacy.

### Disconnect
Not a bad addon, but completely unnecessary if you're uBlock Origin, which you should be.

### Privacy Badger
There's no doubt that Privacy Badger is a good, trustworthy addon. It's made by the EFF and definitely has "protecting your privacy" as its main interest. However, it's not necessary if you're using uMatrix, and if you're using uBlock Origin, Privacy Badger isn't going to catch that many more malicious scripts (this includes trackers).

### NoScript
NoScript and uMatrix overlap in functionality, so it was mostly a choice of one or the other. I prefer uMatrix because NoScript only allows you to allow domains globally -- uMatrix is on a per-site basis -- and uMatrix allows more toggles (cookie/css/media/image/script/etc.) I also think uMatrix is easier to use once you've been using it for a while. That being said, NoScript is a trustworthy addon and some people prefer it over uMatrix.

## user.js tweaks
### privacy.resistFingerprinting =  true
This tweak is the big one. I don't even include it in the "potential breakage" section because it can cause so many issues. A [simple search on the Firefox subreddit](https://www.reddit.com/r/firefox/search?q=fingerprinting&restrict_sr=1) turns up many posts. It can prevent you from installing addons on `addons.mozilla.org`, can break some addons, changes Firefox's timezone to UTC, and forces Firefox to be opened at a certain window size and disallows it from remembering window sizes set after opening, among other issues. 

These issues would be okay if Firefox gave you some kind of warning, like if you couldn't install addons on AMO, Firefox would popup and say, "Hey! It looks like you have `privacy.resistFingerprinting` enabled. That may be causing issues on this site. Click here to disable" or something, but it doesn't. There's no way for someone to know if `privacy.resistFingerprinting` is causing an issue on a website, because it's actually a large number of different tweaks.

### browser.safebrowsing.malware.enabled = false, browser.safebrowsing.phishing.enabled = false
Setting these to false is a bad idea. Firefox NEVER sends your browsing data to Google -- Firefox only downloads blocklists/whitelists from Google and LOCALLY checks your browsing and downloads against those LOCAL lists. From ghacksuserjs' user.js:

> There are NO privacy issues here. *IF* required, a full url is never sent to Google, only a PART-hash of the prefix, and this is hidden with noise of other real PART-hashes. Google also swear it is anonymized and only used to flag malicious sites/activity. Firefox also takes measures such as striping [sic] out identifying parameters and storing safe browsing cookies in a separate jar.

### dom.battery.enabled = false
The battery status API was removed in Firefox 52. You can read the release notes [here](https://www.mozilla.org/en-US/firefox/52.0/releasenotes/).

### network.cookie.lifetimePolicy = 2
I don't want to have to login to all my accounts again when I reopen Firefox.

### network.http.referer.trimmingPolicy = 2, network.http.referer.XOriginPolicy = 2, network.http.referer.XOriginTrimmingPolicy = 2
Privacy Possum takes care of referers.

### privacy.donottrackheader.enabled = true
Hardly any sites respect Do Not Track. Enabling it does nothing on the vast majority of websites. You can read more [here](https://gizmodo.com/do-not-track-the-privacy-tool-used-by-millions-of-peop-1828868324).